# syntax=docker/dockerfile:1
FROM docker:24.0.7-dind as dind
FROM debian:bullseye-slim

ARG ANSIBLE_VERSION=8.6.1

RUN <<EOF
apt-get update
apt-get install --yes --no-install-recommends python3 python3-pip iptables
rm -rf /var/lib/apt/lists/*
EOF

RUN python3 -m pip install --no-cache-dir "ansible==${ANSIBLE_VERSION}" molecule molecule-plugins[docker]

ENV DOCKER_TLS_CERTDIR=/certs
RUN <<EOF
mkdir /certs /certs/client
chmod 1777 /certs /certs/client
EOF

COPY --from=dind /usr/local/bin/ /usr/local/bin/

VOLUME /var/lib/docker

ENTRYPOINT ["dockerd-entrypoint.sh"]
CMD []
